import express from 'express';
const router = express.Router();

import CategoryController from '../controllers/CategoryController';
import authMiddleware from '../middlewares/auth';

/* router.use(authMiddleware); */

router.post('/categories/create', authMiddleware, CategoryController.store);

router.get('/categories', CategoryController.index);

router.delete('/categories/:id', CategoryController.delete);

router.put('/categories/:id', CategoryController.update);

export default router;
