import express from 'express';
const router = express.Router();

import ArticleController from '../controllers/ArticleController';
import authHeader from '../middlewares/auth'

router.use(authHeader)

router.post('/articles/create', ArticleController.store);

router.get('/articles', ArticleController.indexAll ); /* Listar todos as postagens */

router.get('/articles/:category_id', ArticleController.index);

router.delete('/articles/delete/:id', ArticleController.delete);

router.put('/articles/update/:id', ArticleController.update);

export default router;
