import express from 'express';
const router = express.Router();

import AdmController from '../controllers/AdmController';
import authHeader from '../middlewares/auth';

router.use(authHeader);

router.post('/adm/create', AdmController.store);

router.put('/adm/update', AdmController.update);

export default router;
