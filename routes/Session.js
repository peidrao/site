import express from 'express';
const router = express.Router();

import SessionController from '../controllers/SessionController';

router.post('/login', SessionController.store);

export default router;
