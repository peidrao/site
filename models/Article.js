import Sequelize, { Model } from 'sequelize';

class Article extends Model {
  static init(sequelize) {
    super.init(
      {
        title: Sequelize.STRING,
        body: Sequelize.TEXT,
      },
      {
        sequelize,
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.Category, {
      foreignKey: 'category_id',
      as: 'category',
    });
  }
}

export default Article;
