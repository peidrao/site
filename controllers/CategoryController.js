import Category from '../models/Category';
import Adm from '../models/Adm';
import slugfy from 'slugify';
import * as Yup from 'yup';

class CategoryController {
  async index(req, res) {
    try {
      const categories = await Category.findAll({
        attributes: ['id', 'title', 'slug'],
        order: [['id', 'ASC']],
      });
      return res.json(categories);
    } catch (error) {
      console.log(error);
      return res.json(error);
    }
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      title: Yup.string().min(2).required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Insufficient characters' });
    }

    const { title } = req.body;

    const slug = slugfy(title, {
      replacement: '-',
      lower: true,
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Insufficient characters' });
    }

    try {
      if (Adm.findOne({ where: { provider: true } })) {
        const categories = await Category.create({
          title,
          slug,
        });
        return res.json({
          categories,
        });
      }
    } catch (error) {
      console.log(error);
      return res.json({ error: 'Not created' });
    }
  }

  async delete(req, res) {
    const id = req.params.id;

    const idValid = await Category.findByPk(id);

    if (idValid === null) {
      return res.json({ Error: 'id not found' });
    }

    try {
      const idDelete = await Category.destroy({
        where: { id: id },
      });
      if (idDelete) {
        return res.json({ True: 'Category deleted' });
      } else {
        return res.json({ False: 'Category not delete' });
      }
    } catch (error) {
      console.log(error);
    }
  }

  async update(req, res) {
    const { id } = req.params;

    const { title } = req.body;

    const schema = Yup.object().shape({
      title: Yup.string().min(2).required(),
    });

    const slug = await slugfy(title, {
      replacement: '-',
      lower: true,
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Insufficient characters' });
    }

    const category = await Category.findByPk(id);

    const categoryUpdate = await category.update({
      title,
      slug: slug,
    });
    return res.json(categoryUpdate);
  }
}

export default new CategoryController();
