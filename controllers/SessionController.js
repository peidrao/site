import jwt from 'jsonwebtoken';
import * as Yup from 'yup';
import Adm from '../models/Adm';
import auth from '../config/auth';

class SessionController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string().email().required(),
      password: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'validation fails ' });
    }

    const email = req.body.email;
    const password = req.body.password;

    const adm = await Adm.findOne({ where: { email } });

    if (!adm) {
      return res.status(401).json({ error: 'Adm not found ' });
    }

    if (!(await adm.checkPassword(password))) {
      return res.status(401).json({ error: 'Password does not match' });
    }

    const { id, name } = adm;
    /* console.log(id); */
    return res.json({
      adm: {
        id,
        name,
        email,
      },
      token: jwt.sign({ adm: adm.id }, auth.secrect, {
        expiresIn: auth.expiresIn,
      }),
    });
  }
}

export default new SessionController();
