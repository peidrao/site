import Article from '../models/Article';
import Category from '../models/Category';

import * as Yup from 'yup';

class ArticleController {
  async index(req, res) {
    const { category_id } = req.params;

    const category = await Category.findByPk(category_id, {
      include: {
        association: 'articles',
      },
    });

    return res.json(category.articles);
  }

  async indexAll(req, res) {
    try {
      const articles = await Article.findAll();

      if (articles.length == 0) {
        return res.json({ undefined: 'nenhum artigo cadastrado' });
      }

      return res.json(articles);
    } catch (error) {
      console.log(error);
      return res.json(error);
    }
  }

  async store(req, res) {
    const { title, body, category_id } = req.body;

    const schema = Yup.object().shape({
      title: Yup.string().required().min(5),
      body: Yup.string().min(5).required(),
      category_id: Yup.number().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const categoryValid = await Category.findByPk(category_id);
    if (!categoryValid) {
      return res.json({ error: 'Category not found' });
    }

    try {
      const article = await Article.create({ title, body, category_id });

      return res.json(article);
    } catch (error) {
      console.log(error);
      return res.json({ error: 'erro' });
    }
  }

  async delete(req, res) {
    const id = req.params.id;

    const idValid = await Article.findByPk(id);

    if (idValid === null) {
      return res.json({ Error: 'id not found' });
    }

    try {
      const idDelete = await Article.destroy({
        where: { id: id },
      });
      if (idDelete) {
        return res.json({ True: 'Article deleted' });
      } else {
        return res.json({ False: 'Article not delete' });
      }
    } catch (error) {
      console.log(error);
    }
  }

  async update(req, res) {
    const { id } = req.params;

    const { title, body, category_id } = req.body;

    const schema = Yup.object().shape({
      title: Yup.string().min(10).required(),
      body: Yup.string().min(250).required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Invalid' });
    }

    try {
      const article = await Article.findByPk(id);

      const articleUpdate = await article.update({
        title,
        body,
        category_id,
      });
      return res.json(articleUpdate);
    } catch (error) {
      console.log(error);
      return res.json({ error: 'error update' });
    }
  }
}

export default new ArticleController();
