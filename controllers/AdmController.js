import Adm from '../models/Adm';

import * as Yup from 'yup';

class AdmController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      password: Yup.string().required().min(6),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'validation fails ' });
    }

    const admExists = await Adm.findOne({ where: { email: req.body.email } });
    if (admExists) {
      return res.status(400).json({ error: 'User alreasy exists' });
    }
    const { id, name, email } = await Adm.create(req.body);
    return res.json({
      id,
      name,
      email,
    });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string(),
      email: Yup.string().email(),
      oldPassword: Yup.string().min(6),
      password: Yup.string()
        .min(6)
        .when('oldPassword', (oldPassword, field) =>
          oldPassword ? field.required() : field
        ),
      confirmPassword: Yup.string().when('password', (password, field) => {
        password ? field.required().oneOf([Yup.ref('password')]) : field;
      }),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'validation fails ' });
    }

    const { oldPassword, email } = req.body;
    const adm = await Adm.findByPk(req.userId);

    if (email !== adm.email) {
      const admExists = await Adm.findOne({
        where: { email: email },
      });

      if (admExists) {
        return res.status(400).json({ error: 'Adm alreasy exists' });
      }
    }

    if (oldPassword && !(await adm.checkPassword(oldPassword))) {
      return res.status(401).json({ error: 'Password does not match' });
    }

    const { id, name } = await adm.update(req.body);

    return res.json({
      id,
      name,
      email,
    });
  }
}

export default new AdmController();
