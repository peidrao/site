import dbConfig from '../config/database';
import sequelize from 'sequelize';

import Article from '../models/Article';
import Category from '../models/Category';
import Adm from '../models/Adm';

const database = new sequelize(dbConfig);

Article.init(database);
Category.init(database);
Adm.init(database);

Article.associate(database.models);
Category.associate(database.models);

export default database;
