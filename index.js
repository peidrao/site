import express from 'express';
import bodyParser from 'body-parser';
import 'dotenv/config'
const app = express();

import ArticleRoutes from './routes/Article';
import CategoryRoutes from './routes/Category';
import AdmRoutes from './routes/Adm';
import SessionRoutes from './routes/Session';

import './database';

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.json());
app.use('/', SessionRoutes);
app.use('/', AdmRoutes);
app.use('/', ArticleRoutes);
app.use('/', CategoryRoutes);

const port = 3333;
app.listen(port, () => {
  console.log(`Aplicação rodando em ${port}`);
});
